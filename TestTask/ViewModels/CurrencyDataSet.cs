﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Prism.Commands;

namespace TestTask.ViewModels
{
    public class CurrencyDataSet : INotifyPropertyChanged
    {
        public class CurrencyValuteItem : INotifyPropertyChanged
        { 
            private string _Id;
            private string _NumCode;
            private string _CharCode;
            private long _Nominal;
            private string _Name;
            private double _Value;
            private double _Previous;

            [JsonProperty("ID")]
            public string Id { get { return _Id; } set { _Id = value; OnPropertyChanged("Id"); } }
            [JsonProperty("NumCode")]
            public string NumCode { get { return _NumCode; } set { _NumCode = value; OnPropertyChanged("Id"); } }
            [JsonProperty("CharCode")]
            public string CharCode { get { return _CharCode; } set { _CharCode = value; OnPropertyChanged("Id"); } }
            [JsonProperty("Nominal")]
            public long Nominal { get { return _Nominal; } set { _Nominal = value; OnPropertyChanged("Id"); } }
            [JsonProperty("Name")]
            public string Name { get { return _Name; } set { _Name = value; OnPropertyChanged("Id"); } }
            [JsonProperty("Value")]
            public double Value { get { return _Value; } set { _Value = value; OnPropertyChanged("Id"); } }
            [JsonProperty("Previous")]
            public double Previous { get { return _Previous; } set { _Previous = value; OnPropertyChanged("Id"); } }
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged(string propertyName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        [JsonProperty("Date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("PreviousDate")]
        public DateTimeOffset PreviousDate { get; set; }

        [JsonProperty("PreviousURL")]
        public string PreviousUrl { get; set; }

        [JsonProperty("Timestamp")]
        public DateTimeOffset Timestamp { get; set; }

        [JsonProperty("Valute")]
        private Dictionary<string, CurrencyValuteItem> _valute;


        public IEnumerable<CurrencyValuteItem> valuteNames { get { return _valute.Values; } }

        public Dictionary<string, CurrencyValuteItem> val { get => _valute; set => _valute = value; }


        public ICommand searchCommand { get { return new DelegateCommand<string>(msg); } }

        void msg (string text)
        {
            if (text.Trim().Equals("")) return;
            bool found = false;
            foreach (CurrencyValuteItem p in this.val.Values)
            {
                if (text.ToLower().Equals(p.CharCode.ToLower()))
                {
                    found = true;
                    double calcConvertValueUsd = Math.Round((p.Value / val["USD"].Value * Convert.ToDouble(p.Nominal)), 2);
                    double calcConvertValueByn = Math.Round((p.Value / val["BYN"].Value * Convert.ToDouble(p.Nominal)), 2);
                    MessageBox.Show((p.Nominal.ToString() + " " + p.Name.ToString() + " = " + calcConvertValueUsd + " " + val["USD"].CharCode.ToString() + " и " + calcConvertValueByn + " " + val["BYN"].CharCode.ToString()), text.ToUpper(), MessageBoxButton.OK, MessageBoxImage.Information);
                }  
            }
            if (!found)
                MessageBox.Show("Валюта с кодом " + text + " не найдена!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public static CurrencyDataSet FromJson(string json)
        {
            return JsonConvert.DeserializeObject<CurrencyDataSet>(json);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}