﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TestTask.ViewModels;
using static TestTask.ViewModels.CurrencyDataSet;

namespace TestTask
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 

    public interface JSONParserResponseListener
    {
        void JSONParserResponseData(string responseData);
    }

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            UpdateData();
        }

        public void UpdateData()
        {
            string json;
            if (DownloadData("https://www.cbr-xml-daily.ru/daily_json.js", out json))
            {
                DataContext = CurrencyDataSet.FromJson(json);
            }
        }

        bool DownloadData(string url, out string outData)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            string responseData = string.Empty;
            HttpWebResponse httpResponse = (HttpWebResponse)webRequest.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (StreamReader responseReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    outData = responseReader.ReadToEnd();
                    return true;
                }
            }
            else
            {
                outData = "";
                return false;
            }
        }

        private void CalcConvertValute()
        {
            if (valuteComboBox1.Equals("") || valuteComboBox2.Text.Equals("")) return;
            double valueA = 0, valueB = 0;
            CurrencyDataSet tmp = (CurrencyDataSet)DataContext;
            foreach (CurrencyValuteItem p in tmp.val.Values)
            {
                if (valuteComboBox1.Text.Equals(p.Name))
                {
                    valueA = p.Value;
                }
                if (valuteComboBox2.Text.Equals(p.Name))
                {
                    valueB = p.Value;
                }
            }
            convertLabel2.Text = Math.Round((valueA / valueB * Convert.ToDouble(convertLabel1.Text)), 2).ToString();
        }

        public bool IsDigit(string str)
        {
            return double.TryParse(str, out double n);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            Binding binding = new Binding("valuteNames");
            currencyListView.SetBinding(ListView.ItemsSourceProperty, binding);
            UpdateData();
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            Binding binding = new Binding("valuteNames");
            codeValuteListView.SetBinding(ListView.ItemsSourceProperty, binding);
            UpdateData();
        }

        private void ValuteComboBox1_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IsDigit(convertLabel1.Text))
                CalcConvertValute();
        }

        private void ValuteComboBox2_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IsDigit(convertLabel1.Text))
                CalcConvertValute();
        }

        private void ConvertLabel1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsDigit(convertLabel1.Text))
                CalcConvertValute();
        }
    }
}
